FROM node:11 AS build

WORKDIR /code

ADD package.json yarn.lock /code/
RUN yarn install --unsafe-perm

ADD . /code/

RUN yarn build

FROM nginx AS final

COPY --from=build /code/dist /dist
