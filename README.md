
# u2vest


### Project setup
```
yarn  or  npm install
```

#### Compiles and hot-reloads for development
```
yarn run serve   or   npm run serve
```

#### Compiles and minifies for production
```
yarn run build   or   npm run build
```

#### Lints and fixes files
```
yarn run lint   or   npm run lint
```

#### Run your unit tests
```
yarn run test:unit
```

### Components

- vuex
- vue-router
- vue-i18n
- vue-chartjs
- moment
- lodash
- axios
- bootstrap
- bootstrap-vue
- nprogress
- popper.js
- chart.js
