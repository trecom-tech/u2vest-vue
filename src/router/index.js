import Vue from 'vue';
import Router from 'vue-router';
// import store from '../store/store'; see the fixme on line 26
import Registration from '../views/auth/Registration.vue';
import Login from '../views/auth/Login.vue';
import StyleGuide from '../views/design/StyleGuide.vue';
import OnBoarding00 from '../views/onboardings/OnBoarding00.vue';
import OnBoarding01 from '../views/onboardings/OnBoarding01.vue';
import OnBoarding02 from '../views/onboardings/OnBoarding02.vue';
import OnBoarding02Alt from '../views/onboardings/OnBoarding02Alt.vue';
import OnBoarding03 from '../views/onboardings/OnBoarding03.vue';
import OnBoarding04 from '../views/onboardings/OnBoarding04.vue';
import LayoutDashboard from '@/layouts/LayoutDashboard.vue';
import LayoutNotLogged from '@/layouts/LayoutNotLogged.vue';
import EmailVerify from '@/components/Modals/EmailVerify.vue';
import ForgotPassword from '@/components/Modals/ForgotPassword.vue';
import ResetPasswordLinkSent from '@/components/Modals/ResetPasswordLinkSent.vue';
import ResetPassword from '@/components/Modals/ResetPassword.vue';
import Activity from '../views/activity/Activity.vue';
import Funds from '../views/funds/Funds.vue';
import News from '../views/news/News.vue';
import Notifications from '../views/notifications/Notifications.vue';
import NewsSingle from '../views/news/NewsSingle.vue';
import Profile from '../views/auth/Profile.vue';
import Err404 from '../views/404.vue';

Vue.use(Router);

// const lang = store.getters.selectedLocale.locale;
const lang = 'bg'; // fixme

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  scrollBehavior(to) {
    if (to.hash) {
      return {
        selector: to.hash,
      };
    }
    return {
      x: 0,
      y: 0,
    };
  },
  routes: [
    {
      path: '/bg/design',
      name: 'design',
      component: StyleGuide,
      meta: {
        title: 'Style Guide',
      },
    },
    {
      path: '/registration',
      name: 'registration',
      component: Registration,
      meta: {
        title: 'Registration',
      },
    },
    {
      path: '/login',
      name: 'Login',
      component: Login,
      meta: {
        title: 'Login',
      },
    },
    {
      path: '/on-boarding-00',
      component: LayoutNotLogged,
      redirect: '/on-boarding-00',
      children: [
        {
          path: '/on-boarding-00',
          name: 'on_boarding_00',
          component: OnBoarding00,
          meta: {
            progress: 0,
            title: 'On Boarding 00',
          },
        },
        {
          path: '/on-boarding-01',
          name: 'on_boarding_01',
          component: OnBoarding01,
          meta: {
            progress: 25,
            title: 'On Boarding 01',
          },
        },
        {
          path: '/on-boarding-02',
          name: 'on_boarding_02',
          component: OnBoarding02,
          meta: {
            progress: 50,
            title: 'On Boarding 02',
          },
        },
        {
          path: '/on-boarding-02-alt',
          name: 'on_boarding_02_alt',
          component: OnBoarding02Alt,
        },
        {
          path: '/on-boarding-03',
          name: 'on_boarding_03',
          component: OnBoarding03,
          meta: {
            progress: 75,
            title: 'On Boarding 03',
          },
        },
        {
          path: '/on-boarding-04',
          name: 'on_boarding_04',
          component: OnBoarding04,
          meta: {
            progress: 100,
            title: 'On Boarding 04',
          },
        },
      ],
    },
    {
      path: '/',
      component: LayoutDashboard,
      redirect: `/${lang}/funds`,
      children: [
        {
          path: `/${lang}/activity`,
          name: 'activity',
          component: Activity,
          meta: {
            title: 'Activity',
          },
        },
        {
          path: `/${lang}/funds`,
          name: 'funds',
          component: Funds,
          meta: {
            title: 'Funds',
          },
        },
        {
          path: `/${lang}/news`,
          name: 'news',
          component: News,
          meta: {
            title: 'News',
          },
        },
        {
          path: `/${lang}/notifications`,
          name: 'notifications',
          component: Notifications,
          meta: {
            title: 'Notifications',
          },
        },
        {
          path: `/${lang}/news-single`,
          name: 'news_single',
          component: NewsSingle,
          meta: {
            title: 'News Single',
          },
        },
        {
          path: `/${lang}/profile`,
          name: 'profile',
          component: Profile,
          meta: {
            title: 'Profile',
          },
        },
      ],
    },
    {
      path: '/notFound',
      name: 'pageNotFound',
      component: Err404,
      meta: {
        title: '404',
      },
    },
    {
      path: '/verify',
      name: 'verifyEmail',
      component: EmailVerify,
    },
    {
      path: '/forgot-pass',
      name: 'ForgotPassword',
      component: ForgotPassword,
    },
    {
      path: '/reset-link-sent',
      name: 'resetLinkSent',
      component: ResetPasswordLinkSent,
    },
    {
      path: '/reset-password',
      name: 'resetPassword',
      component: ResetPassword,
    },
    // {
    //   path: '*',
    //   redirect: '/notFound',
    // },
    // {
    // path: '/about',
    // name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    // component: () => import(/* webpackChunkName: "about" */ '../views/About.vue'),
    // },
  ],
});

// index.beforeEnter((to, from, next) => {
//   // const lang = to.params.lang;
//   const lang = 'bg';
//   if (!['en', 'bg'].includes(lang)) return next('en');
//
//   return next();
// });

router.beforeEach((to, from, next) => {
  document.title = `u2vest | ${to.meta.title}`;
  if (!to.matched.length) {
    // next('/notFound');
  } else {
    window.Nprogress.start();
    next();
  }
});

router.afterEach(() => {
  document.documentElement.classList.remove('in-menu-mode');
  document.documentElement.classList.remove('in-user-mode');
  window.Nprogress.done();
});

export default router;
