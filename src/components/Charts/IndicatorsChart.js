// Gradient Line Chart
import { Line } from 'vue-chartjs';
import _ from 'lodash';

export default {
  extends: Line,
  data() {
    return {
      options: {
        scales: {
          yAxes: [{
            gridLines: {
              display: true,
              drawBorder: false,
            },
            ticks: {
              // stepSize: 0.001,
              // beginAtZero: false,
              // max: 50,
              // min: 0,
            },
          }],
          xAxes: [{
            type: 'time',
            time: {
              unit: 'week',
            },
            distribution: 'linear',
            gridLines: {
              display: true,
              drawBorder: false,
            },
          }],
        },
        tooltips: {
          mode: 'index',
          intersect: false,
          position: 'nearest',
        },
        legend: {
          display: true,
          position: 'bottom',
          labels: {
            padding: 10,
            fontColor: 'black',
          },
        },
        responsive: true,
        maintainAspectRatio: false,
      },
      dataList: {},
    };
  },
  props: {
    indicatorsData: { // data to plot
      type: Array,
      required: true,
    },
  },
  methods: {
    draw() {
      // colors copied from _variables.scss
      // todo find a way to import named colors from scss
      const colors = ['#4050fb', '#24c592', '#ff6d6d', '#ffb75d'];
      const keys = ['navps', 'purchase', 'redemption'];
      keys.forEach((k) => {
        this.dataList[k] = _.map(this.$props.indicatorsData, k);
      });
      const datasets = Array.from(Object.keys(this.dataList), (key, i) => (
        {
          spanGaps: false,
          label: key,
          lineTension: 0,
          pointBorderWidth: 0,
          pointRadius: 0,
          fill: false,
          backgroundColor: colors[i],
          borderColor: colors[i],
          borderWidth: 2,
          pointHoverRadius: 5,
          pointHoverBackgroundColor: 'red',
          data: Array.from(this.dataList[key], (p, index) => ({
            x: this.$props.indicatorsData[index].date,
            y: p,
          })),
        }));

      this.renderChart({ datasets }, this.options);
    },
    setOptions() {
      const ctx = this.$refs.canvas.getContext('2d');
      const origStroke = ctx.stroke;
      // eslint-disable-next-line
      ctx.stroke = function () {
        ctx.save();
        ctx.shadowColor = 'rgba(63,79,249,0.5)';
        ctx.shadowBlur = 20;
        ctx.shadowOffsetX = 10;
        ctx.shadowOffsetY = 10;
        // ctx.clientHeight = 217;
        origStroke.apply(this);
        ctx.restore();
      };
    },
  },
  mounted() {
    this.setOptions();
    this.draw();
  },
  watch: {
    indicatorsData() {
      this.setOptions();
      this.draw();
    },
  },
};
