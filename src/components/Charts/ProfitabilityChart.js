// Gradient Line ChartJS
import { Line } from 'vue-chartjs';
import _ from 'lodash';
import moment from 'moment';


export default {
  extends: Line,
  data() {
    return {
      options: {
        scales: {
          yAxes: [{
            gridLines: {
              display: true,
              drawBorder: false,
            },
            ticks: {
              // stepSize: 0.001,
              // beginAtZero: false,
            },
          }],
          xAxes: [{
            type: 'time',
            time: {
              unit: 'month',
            },
            distribution: 'linear',
            gridLines: {
              display: true,
              drawBorder: false,
            },
          }],
        },
        tooltips: {
          mode: 'index',
          intersect: false,
          position: 'nearest',
        },
        legend: {
          display: true,
          position: 'top',
          labels: {
            padding: 10,
            fontColor: 'black',
          },
        },
        responsive: true,
        maintainAspectRatio: false,
      },
    };
  },
  props: [
    'profitability',
    'fundsObj',
  ],
  methods: {
    setOptions() {
      const ctx = this.$refs.canvas.getContext('2d');
      const origStroke = ctx.stroke;
      // eslint-disable-next-line
      ctx.stroke = function () {
        ctx.save();
        // todo find a way to import named colors from scss
        ctx.shadowColor = 'rgba(63,79,249,0.5)';
        ctx.shadowBlur = 10;
        ctx.shadowOffsetX = 10;
        ctx.shadowOffsetY = 10;
        // ctx.clientHeight = 217;
        origStroke.apply(this);
        ctx.restore();
      };
    },
  },
  mounted() {
    // colors copied from _variables.scss
    // todo find a way to import named colors from scss
    const colors = ['#4050fb', '#24c592', '#ff6d6d', '#ffb75d'];


    this.setOptions();
    const profitabilities = _.groupBy(this.profitability, 'fund_id');
    const datasets = Array.from(Object.keys(profitabilities), key => (
      {
        spanGaps: false,
        label: this.fundsObj[key].name_bg,
        lineTension: 0,
        pointBorderWidth: 0,
        pointRadius: 0,
        fill: false,
        backgroundColor: colors[key],
        borderColor: colors[key],
        borderWidth: 2,
        data: Array.from(profitabilities[key], p => ({
          x: moment(p.date).format('YYYY-MM-DD'),
          y: p.profitability,
        })),
      }));

    this.renderChart({ datasets }, this.options);
  },
};
