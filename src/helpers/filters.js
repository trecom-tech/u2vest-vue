import Vue from 'vue';
import moment from 'moment';
import numeral from 'numeral';

// fixme: locale specific date formatting
// Vue.filter('formatDate', value => moment(String(value)).format('DD MMMM YYYY'));
Vue.filter('formatDate',
  value => (value ? moment(value).format('DD MMMM YYYY') : '0000-00-00'));

Vue.filter('numFormat',
  (value, fmt) => numeral(value).format(fmt));
