import Vue from 'vue';
import Vuex from 'vuex';

// modules
import auth from './modules/auth';
import settings from './modules/settings';
import profile from './modules/profile';
import { languages } from './modules/settings/data';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    auth,
    settings,
    profile,
  },
  /* Global state */
  state: {
    languages,
    selectedLocale: {
      name: 'Bulgarian',
      locale: 'bg',
    },
  },
});
