export const languages = [
  {
    name: 'English',
    icon: 'us',
    locale: 'en',
  },
  {
    name: 'Bulgarian',
    icon: 'bg',
    locale: 'bg',
  },
];

export default languages;
