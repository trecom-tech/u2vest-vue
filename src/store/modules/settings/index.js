/**
 * Settings Module
 */
import axios from 'axios';

function filterListByFundId(list, filterVal) {
  if (!filterVal || !list) return list; // guard against undefined list or no filter
  return list.filter(item => item.fund_id === filterVal);
}


export default {
  namespaced: true,
  state: {
    // Funds list
    funds: [],
    // Funds dictionary, convenience data structure
    fundsObj: {},
    // ALL Funds Indicators list
    indicators: [],
    indicatorsLatest: [],
    // ALL funds' securities
    securities: [],
    // User's profitability data
    profitability: [],
    // User's owned units
    units: [],
    // List of all fund and units owned in each by the user
    owned: [],
    // Current selected fund id and name from the dropdown in the Funds component
    // Should not be set on the store, but due to convenience it will live here
    // fund ids are the ones that the backend is sending
    selectedFundId: 1,
    selectedFundName: '',
  },
  actions: {
    fetchFunds(context) {
      axios
        .get('/api/v1/funds/')
        .then((res) => {
          context.commit('FUNDS_SUCCESS', res.data);
          context.commit('CALCULATE_OWNED');
        })
        .catch(err => console.error(err));
    },
    fetchIndicators(context, fundId) {
      axios
        .get(`/api/v1/indicators/${fundId}/?latest=1`)
        .then((res) => {
          context.commit('INDICATORS_SUCCESS', res.data);
        })
        .catch(err => console.error(err));
    },
    fetchIndicatorsLatest(context) {
      axios
        .get('/api/v1/indicators/?latest=1')
        .then((res) => {
          context.commit('INDICATORS_LATEST_SUCCESS', res.data);
        })
        .catch(err => console.error(err));
    },
    fetchIndicatorsAll(context) {
      axios
        .get('/api/v1/indicators/')
        .then((res) => {
          context.commit('INDICATORS_ALL_SUCCESS', res.data);
        })
        .catch(err => console.error(err));
    },
    fetchProfitability(context) {
      axios
        .get('/api/v1/profitability/')
        .then((res) => {
          context.commit('PROFITABILITY_SUCCESS', res.data);
        })
        .catch(err => console.error(err));
    },
    fetchUnits(context) {
      axios
        .get('/api/v1/units/')
        .then((res) => {
          context.commit('UNITS_SUCCESS', res.data);
          context.commit('CALCULATE_OWNED');
        })
        .catch(err => console.error(err));
    },
    fetchSecurities(context, fundId) {
      axios
        .get(`/api/v1/securities/${fundId}/`)
        .then((res) => {
          context.commit('SECURITIES_SUCCESS', res.data);
        })
        .catch(err => console.error(err));
    },
    fetchSecuritiesAll(context) {
      axios
        .get('/api/v1/securities/')
        .then((res) => {
          context.commit('SECURITIES_ALL_SUCCESS', res.data);
        })
        .catch(err => console.error(err));
    },
    setFund(context, fundId) {
      if (context.state.fundsObj) {
        const fund = context.state.fundsObj[fundId];
        console.debug(`Selected listid:${fundId} fund_id:${fund.id} name_bg:"${fund.name_bg}"}`);
        context.commit('FUND_CHANGED', {
          fundId: fund.id,
          fundName: fund.name_bg,
        });
      } else {
        console.error('No funds yet!');
      }
    },
  },
  mutations: {
    FUNDS_SUCCESS(state, val) {
      state.fundsObj = val.reduce((obj, item) => {
        obj[item.id] = item;
        return obj;
      }, {});
      state.funds = val;
      state.selectedFundName = state.fundsObj[state.selectedFundId].name_bg;
    },
    INDICATORS_SUCCESS(state, val) {
      // Individual fund indicators update event
      console.debug(state, val);
      // state.indicators = val;
    },
    INDICATORS_ALL_SUCCESS(state, val) {
      state.indicators = val;
    },
    INDICATORS_LATEST_SUCCESS(state, val) {
      state.indicatorsLatest = val;
    },
    PROFITABILITY_SUCCESS(state, val) {
      state.profitability = val;
    },
    SECURITIES_SUCCESS(state, val) {
      // Individual fund securities update event
      // state.securities = val;
      console.debug(state, val);
    },
    SECURITIES_ALL_SUCCESS(state, val) {
      state.securities = val;
    },
    UNITS_SUCCESS(state, val) {
      state.units = val;
    },
    CALCULATE_OWNED(state) {
      if (state.units && state.fundsObj) {
        // prefill with all zeroes
        state.owned = state.funds.map(fund => ({
          name_bg: fund.name_bg,
          fund_id: fund.id,
          assets: 0,
          units: 0,
        }));

        state.units.forEach((unit) => {
          const f = state.owned.find(o => o.fund_id === unit.fund_id);
          if (f) {
            f.assets = unit.assets;
            f.units = unit.units;
          }
        });
      }
    },
    FUND_CHANGED(state, payload) {
      state.selectedFundId = payload.fundId;
      state.selectedFundName = payload.fundName;
    },
  },
  getters: {
    indicatorsFiltered(state) {
      return filterListByFundId(state.indicators, state.selectedFundId);
      // return state.indicators.filter(ind => ind.fund_id === state.selectedFundId);
    },
    indicatorsLatestFiltered(state) {
      return filterListByFundId(state.indicatorsLatest, state.selectedFundId)[0];
      // return state.indicatorsLatest.filter(ind => ind.fund_id === state.selectedFundId)[0];
    },
    securitiesFiltered(state) {
      return filterListByFundId(state.securities, state.selectedFundId);
      // return state.securities.filter(sec => sec.fund_id === state.selectedFundId);
    },
    fundsReady(state) {
      return Boolean(state.funds.length);
    },
    indicatorsReady(state) {
      console.debug(`indicators ready ${Boolean(state.indicators.length)}`);
      return Boolean(state.indicators.length);
    },
    indicatorsLatestReady(state) {
      console.debug(`indicators LATEST ready ${Boolean(state.indicatorsLatest.length)}`);
      return Boolean(state.indicatorsLatest.length);
    },
    securitiesReady(state) {
      console.debug(`securities ready ${Boolean(state.securities.length)}`);
      return Boolean(state.securities.length);
    },
    profitabilityReady(state) {
      console.debug(`profitability ready ${Boolean(state.profitability.length)}`);
      return Boolean(state.profitability.length);
    },
  },
};
