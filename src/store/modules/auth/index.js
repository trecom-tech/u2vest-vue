/*
* Auth module
* */

import axios from 'axios';
import Nprogress from 'nprogress';

const apiUrl = '/api/v1';
axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

export default {
  state: {
    approximateSavings: '',
    educationDegree: '',
    irregularIncome: '',
    liabilities: '',
    marketValueOfProperty: '',
    monthlyIncome: '',
    monthlyIncomeSource: '',
    onBoardingData: {},
    otherIncomeSource: '',
    ownProperty: '',
    savings: '',
    sumOfLiabilities: '',
    totalSumOfAssets: '',
    user: localStorage.getItem('u2testUser'),
    validationErrors: {},
    yearlyIrregularIncome: '',
  },
  getters: {
    approximateSavings: state => state.approximateSavings,
    educationDegree: state => state.educationDegree,
    getUser: state => state.user,
    irregularIncome: state => state.irregularIncome,
    liabilities: state => state.liabilities,
    marketValueOfProperty: state => state.marketValueOfProperty,
    monthlyIncome: state => state.monthlyIncome,
    monthlyIncomeSource: state => state.monthlyIncomeSource,
    onBoardingData: state => state.onBoardingData,
    otherIncomeSource: state => state.otherIncomeSource,
    ownProperty: state => state.ownProperty,
    savings: state => state.savings,
    sumOfLiabilities: state => state.sumOfLiabilities,
    totalSumOfAssets: state => state.totalSumOfAssets,
    validationErrors: state => state.validationErrors,
    yearlyIrregularIncome: state => state.yearlyIrregularIncome,
  },
  actions: {
    approximateSavings(context, payload) {
      context.commit('approximateSavings', payload);
    },
    educationDegree(context, payload) {
      context.commit('educationDegree', payload);
    },
    irregularIncome(context, payload) {
      context.commit('irregularIncome', payload);
    },
    liabilities(context, payload) {
      context.commit('liabilities', payload);
    },
    marketValueOfProperty(context, payload) {
      context.commit('marketValueOfProperty', payload);
    },
    monthlyIncome(context, payload) {
      context.commit('monthlyIncome', payload);
    },
    monthlyIncomeSource(context, payload) {
      context.commit('monthlyIncomeSource', payload);
    },
    onBoardingData(context, payload) {
      context.commit('onBoardingData', payload);
    },
    otherIncomeSource(context, payload) {
      context.commit('otherIncomeSource', payload);
    },
    ownProperty(context, payload) {
      context.commit('ownProperty', payload);
    },
    savings(context, payload) {
      context.commit('savings', payload);
    },
    sumOfLiabilities(context, payload) {
      context.commit('sumOfLiabilities', payload);
    },
    totalSumOfAssets(context, payload) {
      context.commit('totalSumOfAssets', payload);
    },
    yearlyIrregularIncome(context, payload) {
      context.commit('yearlyIrregularIncome', payload);
    },
    onBoard(context, payload) {
      const { onBoardingData, router, targetRouter } = payload;
      Nprogress.start();
      console.debug('data: ', onBoardingData);
      axios
        .post(`${apiUrl}/accounts/onboard/`, onBoardingData)
        .then((res) => {
          console.debug(res);
          Nprogress.done();
          router.push(targetRouter);
        })
        .catch(err => console.error(err.response.data));
    },
    signInUser(context, payload) {
      const { user, router } = payload;
      context.commit('loginUser');

      axios
        .post(`${apiUrl}/accounts/login/`, user)
        .then((res) => {
          console.debug(res);
          context.commit('loginUserSuccess', user, router);
          // router.push('/bg/funds');
          axios
            .get(`${apiUrl}/accounts/profile/`)
            .then((response) => {
              if (response.data.onboarded) {
                router.push('/bg/funds');
              } else {
                router.push('/on-boarding-00');
              }
            })
            .catch((err) => {
              console.error(err);
              router.push('/on-boarding-00');
            });
          Nprogress.done();
        })
        .catch((error) => {
          context.commit('loginUserFailure', error.response.data);
        });
    },
    signUpUser(context, payload) {
      const { userDetail, router } = payload;
      context.commit('signUpUser');

      axios
        .post(`${apiUrl}/accounts/register/`, userDetail)
        .then((res) => {
          console.debug(res);
          Nprogress.done();
          context.commit('signUpUserSuccess', userDetail);
          router.push('/login');
        })
        .catch((error) => {
          console.error(error.response.status);
          console.error(error.response.data);
          context.commit('signUpUserFailure', error.response.data);
        });
    },
    logoutUser(context, router) {
      Nprogress.start();
      Nprogress.done();
      context.commit('logoutUser');
      localStorage.removeItem('u2testUser');
      router.push('/login');
    },
    emailVerify(context, payload) {
      const { userDetail, router } = payload;

      axios
        .post(`${apiUrl}/accounts/verify-registration/`, userDetail)
        .then((res) => {
          console.debug(res);
          Nprogress.done();
          context.commit('emailVerifySuccess', userDetail);

          axios
            .get(`${apiUrl}/accounts/profile/`)
            .then((response) => {
              console.debug(response);
              if (response.data.onboarded) {
                router.push('/bg/funds');
              } else {
                router.push('/on-boarding-00');
              }
            })
            .catch((err) => {
              console.error(err);
              router.push('/on-boarding-00');
            });
          // router.push('/bg/funds');
        });
    },
    forgotPassword(context, payload) {
      const { userDetail, router } = payload;

      axios
        .post(`${apiUrl}/accounts/send-reset-password-link/`, userDetail)
        .then(() => {
          Nprogress.done();
          // context.commit('resetPasswordLinkSent', userDetail);
          router.push('/reset-link-sent');
        })
        .catch(() => {
          // context.commit('resetPasswordLinkFailed', error.response.data);
        });
    },
    resetPassword(context, payload) {
      const { Detail, router } = payload;

      axios
        .post(`${apiUrl}/accounts/reset-password/`, Detail)
        .then(() => {
          Nprogress.done();
          // context.commit('resetPasswordLinkSent', userDetail);
          router.push('/login');
        })
        .catch(() => {
          // context.commit('resetPasswordLinkFailed', error.response.data);
        });
    },
  },
  mutations: {
    approximateSavings(state, val) {
      state.approximateSavings = val;
    },
    educationDegree(state, val) {
      state.educationDegree = val;
    },
    irregularIncome(state, val) {
      state.irregularIncome = val;
    },
    liabilities(state, val) {
      state.liabilities = val;
    },
    marketValueOfProperty(state, val) {
      state.marketValueOfProperty = val;
    },
    monthlyIncome(state, val) {
      state.monthlyIncome = val;
    },
    monthlyIncomeSource(state, val) {
      state.monthlyIncomeSource = val;
    },
    onBoardingData(state, val) {
      state.onBoardingData = val;
    },
    otherIncomeSource(state, val) {
      state.otherIncomeSource = val;
    },
    ownProperty(state, val) {
      state.ownProperty = val;
    },
    savings(state, val) {
      state.savings = val;
    },
    sumOfLiabilities(state, val) {
      state.sumOfLiabilities = val;
    },
    totalSumOfAssets(state, val) {
      state.totalSumOfAssets = val;
    },
    yearlyIrregularIncome(state, val) {
      state.yearlyIrregularIncome = val;
    },
    loginUser() {
      Nprogress.start();
    },
    loginUserSuccess(state, user) {
      state.user = localStorage.setItem('u2testUser', user);
    },
    loginUserFailure(state, details) {
      state.validationErrors = details;
      Nprogress.done();
    },
    logoutUser(state) {
      state.user = null;
    },
    signUpUser() {
      Nprogress.start();
    },
    signUpUserSuccess(state, user) {
      state.user = localStorage.setItem('u2testUser', JSON.stringify(user));
    },
    signUpUserFailure(state, details) {
      state.validationErrors = details;
      Nprogress.done();
    },
    emailVerifySuccess(state, user) {
      state.user = localStorage.setItem('u2testUser', JSON.stringify(user));
    },
  },
};
