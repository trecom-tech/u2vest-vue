/**
 * Profile Module
 */
import axios from 'axios';

export default {
  state: {
    // User's profile
    profile: {},
  },
  getters: {
    profile: state => state.profile,
  },
  actions: {
    async fetchProfile(context) {
      await axios
        .get('/api/v1/accounts/profile/')
        .then((res) => {
          context.commit('PROFILE_SUCCESS', res.data);
        })
        .catch(err => console.error(err));
    },
    async updateProfile(context, data) {
      await axios
        .put('/api/v1/accounts/profile/', data)
        .then((res) => {
          context.commit('PROFILE_UPDATE_SUCCESS', res.data);
        });
    },
  },
  mutations: {
    PROFILE_SUCCESS(state, val) {
      state.profile = val;
    },
    PROFILE_UPDATE_SUCCESS(state, val) {
      state.profile = val;
    },
  },
};
