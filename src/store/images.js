import iconFunds from '@/assets/img/menu/icon-funds.svg';
import iconActivity from '@/assets/img/menu/icon-activity.svg';
import iconNotification from '@/assets/img/menu/icon-notification.svg';
import iconNews from '@/assets/img/menu/icon-news.svg';

export default {
  iconFunds,
  iconActivity,
  iconNotification,
  iconNews,
};
