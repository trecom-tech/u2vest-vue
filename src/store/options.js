const irregularYearlyIncome = [
  { text: 'До 10 000 лв', value: 'to_10000' },
  { text: 'От 10 000 до 50 000 лв', value: 'to_50000' },
  { text: 'Над 50 000 лв', value: 'from_50000' },
];

const irregularSourceOfIncome = [
  { text: 'Доходи от свободна професия', value: 'self_employed' },
  { text: 'Доходи от собствен бизнес', value: 'company' },
  { text: 'Доходи от инвестиции и лихви', value: 'investment' },
  { text: 'Други - моля посочете', value: 'others' },
];

const monthlyIncome = [
  { text: 'От 500 до 1000 лв', value: 'to_500' },
  { text: 'От 1000 до 2000 лв', value: 'to_2000' },
  { text: 'От 2000 до 3000 лв', value: 'to_3000' },
  { text: 'От 3000 до 4000 лв', value: 'to_4000' },
  { text: 'От 4000 до 5000 лв', value: 'to_5000' },
  { text: 'От 5000 до 6000 лв', value: 'to_6000' },
  { text: 'От 6000 до 7000 лв', value: 'to_7000' },
  { text: 'От 7000 до 10000 лв', value: 'to_10000' },
  { text: 'Над 10000 лв', value: 'from_10000' },
];

const mainSourceOfIncome = [
  { text: 'Трудови възнаграждения и/или хонорари', value: 'employment' },
  { text: 'Социални плащания (пенсии, стипендии и подобни)', value: 'social' },
  { text: 'Доходи от наеми/аренди', value: 'rent' },
  { text: 'Лихви и/или дивиденти от инвестиции', value: 'interest' },
  { text: 'Други', value: 'others' },
];

const savings = [
  { text: 'Имам', value: 'Имам' },
  { text: 'Нямам', value: 'Нямам' },
];

const savingsAmount = [
  { text: 'До 10 000 лв', value: 'to_10000' },
  { text: 'От 10 000 до 100 000 лв', value: 'to_100000' },
  { text: 'От 100 000 до 200 000 лв', value: 'to_200000' },
  { text: 'От 200 000 до 500 000 лв', value: 'to_500000' },
  { text: 'От 500 000 до 1 000 000 лв', value: 'to_1000000' },
  { text: 'Над 1 000 000 лв', value: 'from_1000000' },
];

const property = [
  { text: 'Притежавам', value: 'Притежавам' },
  { text: 'Не притежавам', value: 'Не притежавам' },
];

const propertyAmount = [
  { text: 'до 300 000лв', value: 'to_300000' },
  { text: 'от 300 000 до 1 000 000 лв', value: 'to_1000000' },
  { text: 'Между 1 000 000 и 2 000 000 лв', value: 'to_2000000' },
  { text: 'Над 2 000 000 лв', value: 'from_2000000' },
];

const liabilities = [
  { text: 'Имам', value: 'Имам' },
  { text: 'Нямам', value: 'Нямам' },
];

const assetsAmount = [
  { text: 'До 10 000 лв', value: 'to_10000' },
  { text: 'От 10 000 до 100 000 лв', value: 'to_100000' },
  { text: 'От 100 000 до 200 000 лв', value: 'to_200000' },
  { text: 'От 200 000 до 500 000 лв', value: 'to_500000' },
  { text: 'От 500 000 до 1 000 000 лв', value: 'to_1000000' },
  { text: 'Над 1 000 000 лв', value: 'from_1000000' },
];

const educations = [
  { text: 'основно', value: 'primary' },
  { text: 'средно', value: 'secondary' },
  { text: 'бакалавър', value: 'bachelors' },
  { text: 'магистър', value: 'masters' },
  { text: 'доктор на науките', value: 'doctorate' },
  { text: 'друго - моля посочете', value: 'other' },
];

export {
  assetsAmount,
  educations,
  irregularSourceOfIncome,
  irregularYearlyIncome,
  liabilities,
  mainSourceOfIncome,
  monthlyIncome,
  property,
  propertyAmount,
  savings,
  savingsAmount,
};
