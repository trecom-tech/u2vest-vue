import en from './en';
import bg from './bg';

export default {
  en: {
    message: en,
  },
  bg: {
    message: bg,
  },
};
