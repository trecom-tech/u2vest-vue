/**
 * Change all chart colors
 */
// import AppConfig from './AppConfig';

export const ChartConfig = {
  color: {
    start: '#9399c6',
    stop: '#ffffff',
  },
  legendFontColor: '#AAAEB3', // only works on react chart js 2
  chartGridColor: '#EAEAEA',
  axesColor: '#657786',
  shadowColor: 'rgba(0,0,0,0.5)',
};

export default ChartConfig;
