
import { Chart } from 'vue-chartjs';

// Colors
const colors = {
  moneyGreen: '#00aa6e',
  businessBlue: '#4c84ff',
  darkishPink: '#e24358',
  gray: '#858c9a',
  purple: '#999dc6',
};

// Global Charts settings

Chart.defaults.global.defaultFontColor = colors.gray;
Chart.defaults.global.defaultFontFamily = 'Roboto';
Chart.defaults.global.defaultFontSize = 16;
Chart.defaults.global.legend.usePointStyle = true;
Chart.defaults.global.elements.point.backgroundColor = 'rgba(0, 0, 0, 0)';
Chart.defaults.global.elements.point.borderColor = 'rgba(0, 0, 0, 0)';
Chart.defaults.global.tooltips.mode = 'point';
Chart.defaults.global.maintainAspectRatio = false;

// Get contexts
const accountActivity = document.getElementById('account-activity')
  .getContext('2d');
const myProfit = document.getElementById('my-profit')
  .getContext('2d');
const exmpleInvestment = document.getElementById('example-investment')
  .getContext('2d');

// Gradients
const greenGradient = accountActivity.createLinearGradient(0, 0, 0, 500);
greenGradient.addColorStop(0, 'rgba(0, 170, 110, 0.16)');
greenGradient.addColorStop(1, 'rgba(0, 170, 110, 0)');

const blueGradient = accountActivity.createLinearGradient(0, 0, 0, 500);
blueGradient.addColorStop(0, 'rgba(76, 132, 255, 0.16)');
blueGradient.addColorStop(1, 'rgba(76, 132, 255, 0)');

const redGradient = accountActivity.createLinearGradient(0, 0, 0, 500);
redGradient.addColorStop(0, 'rgba(226, 67, 88, 0.16)');
redGradient.addColorStop(1, 'rgba(226, 67, 88, 0)');

const purpleGradient = accountActivity.createLinearGradient(0, 0, 0, 500);
redGradient.addColorStop(0, 'rgba(32, 42, 147, 0.16)');
redGradient.addColorStop(1, 'rgba(32, 42, 147, 0)');

// Account activity chart
const accountActivityChart = new Chart(accountActivity, { // eslint-disable-line no-unused-vars
  type: 'line',
  data: {
    labels: ['Sept', 'Oct', 'Nov', 'Dec', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug'],
    datasets: [
      {
        label: 'Added',
        data: [1, 0.8, 1, 0.55, 0.65, 0.3, 0.4, 0.5, 0.7, 0.3, 0.2, 0.5],
        backgroundColor: purpleGradient,
        borderColor: colors.purple,
        borderWidth: 2,
        lineTension: 0,
      },
    ],
  },
  options: {
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true,
        },
      }],
    },
  },
});

// Example investment
const exmpleInvestmentChart = new Chart(exmpleInvestment, { // eslint-disable-line no-unused-vars
  type: 'line',
  data: {
    labels: ['Sept', 'Oct', 'Nov', 'Dec', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug'],
    datasets: [
      {
        label: 'Added',
        data: [1, 0.8, 1, 0.55, 0.65, 0.3, 0.4, 0.5, 0.7, 0.3, 0.2, 0.5],
        backgroundColor: purpleGradient,
        borderColor: colors.purple,
        borderWidth: 2,
        lineTension: 0,
      },
    ],
  },
  options: {
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true,
        },
      }],
    },
  },
});

// My profit
const myProfitChart = new Chart(myProfit, { // eslint-disable-line no-unused-vars
  type: 'line',
  data: {
    labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'],
    datasets: [
      {
        label: 'Smart Tech',
        data: [5000, 5500, 6000, 7000, 7500, 8000, 5000, 6000, 8000, 9000, 4000, 10000],
        backgroundColor: purpleGradient,
        borderColor: colors.businessBlue,
        borderWidth: 2,
        lineTension: 0,
      },
      {
        label: 'Алфа Sofix Индекс',
        data: [4000, 6500, 3000, 5000, 6500, 4000, 7000, 3000, 6000, 8000, 7000, 8000],
        backgroundColor: purpleGradient,
        borderColor: colors.moneyGreen,
        borderWidth: 2,
        lineTension: 0,
      },
    ],
  },
  options: {
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true,
        },
      }],
    },
  },
});
