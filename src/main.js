import Vue from 'vue';
import VueI18n from 'vue-i18n';
import BootstrapVue from 'bootstrap-vue';
import Datepicker from 'vuejs-datepicker';
// import MaskedInput from 'vue-text-mask';
import VeeValidate from 'vee-validate';
import VueCookies from 'vue-cookies';
import * as Sentry from '@sentry/browser';
import * as Integrations from '@sentry/integrations';

import App from './App.vue';
import router from './router';

// store
import store from './store/store';

// messages
import messages from './lang';

/* filters */
import './helpers/filters';

/* import nprogress */
import Nprogress from '../node_modules/nprogress';

/* import stylesheet */
import './assets/scss/style.scss';

Nprogress.configure({ showSpinner: false });
window.Nprogress = Nprogress;

// localization
Vue.use(VueI18n);

// import bootstrap-vue
Vue.use(BootstrapVue);

// import date picker
Vue.use(Datepicker);

// import cookies
Vue.use(VueCookies);

// import vee validator and customize
Vue.use(VeeValidate);
VeeValidate.Validator.extend('verify_password', {
  validate: (value) => {
    const strongRegex = new RegExp('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})');
    return strongRegex.test(value);
  },
});
VeeValidate.Validator.extend('full_name', {
  validate: (value) => {
    const strongRegex = new RegExp('^[\\w\\W]+ [\\w\\W]');
    return strongRegex.test(value);
  },
});

// Vue.component('masked-input', MaskedInput);

Vue.config.errorHandler = (err, vm, info) => {
  console.error(err, info);
};

Sentry.init({
  dsn: 'https://bd4832cc1e6a4b188bfbbb436e1a4f75@sentry.io/1441325',
  integrations: [
    new Integrations.Vue({
      Vue,
      attachProps: true,
    }),
  ],
});

Vue.config.productionTip = false;

// Create VueI18n instance with options
const i18n = new VueI18n({
  locale: 'bg', // set locale
  messages, // set locale messages
});

new Vue({
  router,
  store,
  i18n,
  render: h => h(App),
}).$mount('#app');
