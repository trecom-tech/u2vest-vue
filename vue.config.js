const SentryWebpackPlugin = require('@sentry/webpack-plugin');
const fs = require('fs');

let configureWebpack;

try {
  configureWebpack = {
    plugins: [
      new SentryWebpackPlugin({
        include: '.',
        ignore: ['node_modules', 'webpack.config.js'],
        release: fs.readFileSync('githash', 'utf8'),
      }),
    ],
  };
} catch (e) {
  configureWebpack = {};
}

const targetURL = new URL('https://test.u2vest.com/');

const proxyOptions = {
  target: targetURL.href,
  onProxyReq(proxyReq, req, res) {
    if (req.headers.referer) {
      let referrer = new URL(req.headers.referer);
      if (`${referrer.hostname}:${referrer.port}` == req.headers.host) {
        referrer.hostname = targetURL.hostname;
        referrer.protocol = targetURL.protocol;
        referrer.port = targetURL.port;
        proxyReq.setHeader('referer', referrer.href)
      }
    }
  },
};

module.exports = {
  devServer: {
    proxy: {
      '^/admin/': proxyOptions,
      '^/api/': proxyOptions,
      '^/static/': proxyOptions,
    },
  },
  configureWebpack,
};
